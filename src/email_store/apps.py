from django.apps import AppConfig


class EmailStoreConfig(AppConfig):
    name = "email_store"
